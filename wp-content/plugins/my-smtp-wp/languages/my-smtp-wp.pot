#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: MY SMTP WP\n"
"POT-Creation-Date: 2016-02-24 23:43-0300\n"
"PO-Revision-Date: 2016-02-24 17:16-0300\n"
"Last-Translator: Valério Souza <eu@valeriosouza.com.br>\n"
"Language-Team: \n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.4\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-KeywordsList: _e;__;_x\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: .\n"

#: class-my-smtp-wp.php:28
msgid "The field \"From Email\" must be a valid email address!"
msgstr ""

#: class-my-smtp-wp.php:31
msgid "The field \"SMTP Host\" can not be left blank!"
msgstr ""

#: class-my-smtp-wp.php:34
msgid "Options saved."
msgstr ""

#: class-my-smtp-wp.php:47
msgid "Please enter a valid email address in the 'FROM' field."
msgstr ""

#: class-my-smtp-wp.php:110
msgid "[My SMTP WP] Your plugin is working"
msgstr ""

#: class-my-smtp-wp.php:111
msgid ""
"If you are reading this email, it is because your plugin is successfully "
"configured."
msgstr ""

#: class-my-smtp-wp.php:148
msgid "Some errors occurred! Check the settings!"
msgstr ""

#: class-my-smtp-wp.php:153
msgid "Message sent successfully!"
msgstr ""

#: class-my-smtp-wp.php:163
msgid "My Help Tab"
msgstr ""

#: class-my-smtp-wp.php:164
msgid "Descriptive content that will show in My Help Tab-body goes here."
msgstr ""

#: my-smtp-wp-admin.php:12
msgid "Need help setting up?"
msgstr ""

#: my-smtp-wp-admin.php:12
msgid "Check how!"
msgstr ""

#: my-smtp-wp-admin.php:13
msgid "Using Gmail and getting error?"
msgstr ""

#: my-smtp-wp-admin.php:13
msgid "Authorize low security with applications"
msgstr ""

#: my-smtp-wp-admin.php:13
msgid "or"
msgstr ""

#: my-smtp-wp-admin.php:13
msgid "Create a application password for the plugin."
msgstr ""

#: my-smtp-wp-admin.php:20
msgid "From Email"
msgstr ""

#: my-smtp-wp-admin.php:30
msgid "From Name"
msgstr ""

#: my-smtp-wp-admin.php:40
msgid "Reply To"
msgstr ""

#: my-smtp-wp-admin.php:50
msgid "SMTP Options"
msgstr ""

#: my-smtp-wp-admin.php:55
msgid "SMTP Host"
msgstr ""

#: my-smtp-wp-admin.php:65
msgid "SMTP Encryption"
msgstr ""

#: my-smtp-wp-admin.php:70
msgid "No encryption"
msgstr ""

#: my-smtp-wp-admin.php:75
msgid "Use SSL encryption"
msgstr ""

#: my-smtp-wp-admin.php:79
msgid "Use TLS encryption"
msgstr ""

#: my-smtp-wp-admin.php:85
msgid "SMTP Port"
msgstr ""

#: my-smtp-wp-admin.php:95
msgid "SMTP Authentication"
msgstr ""

#: my-smtp-wp-admin.php:100
msgid "No: Do not use SMTP authentication"
msgstr ""

#: my-smtp-wp-admin.php:104
msgid "Yes: Use SMTP authentication"
msgstr ""

#: my-smtp-wp-admin.php:110
msgid "Username ( Email Address )"
msgstr ""

#: my-smtp-wp-admin.php:120
msgid "Password ( Email Address )"
msgstr ""

#: my-smtp-wp-admin.php:131
msgid "Active Return-Path Header Fix?"
msgstr ""

#: my-smtp-wp-admin.php:136
msgid "No: Do not use Return-Path"
msgstr ""

#: my-smtp-wp-admin.php:140
msgid "Yes: Use Return-Path"
msgstr ""

#: my-smtp-wp-admin.php:150
msgid "Save Changes"
msgstr ""

#: my-smtp-wp-admin.php:159
msgid "Send a Test Email"
msgstr ""

#: my-smtp-wp-admin.php:164
msgid "To:"
msgstr ""

#: my-smtp-wp-admin.php:170
msgid ""
"Type an email address here and then click Send Test to generate a test email."
msgstr ""

#: my-smtp-wp-admin.php:177
msgid "Send Test"
msgstr ""

#: my-smtp-wp.php:92
msgid "Settings"
msgstr ""

#: my-smtp-wp.php:103
msgid "Donate"
msgstr ""
